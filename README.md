# Bolero de Ravel - apreciação musical

O Bolero de Ravel, obra composta entre Julho e Outubro de 1928 no Tempo di Bolero, moderato assai ("tempo de bolero, muito moderado"), tem um ritmo invariável e uma melodia uniforme e repetitiva. Deste modo, a única sensação de mudança é dada pelos efeitos de orquestração e dinâmica, com um crescendo progressivo é uma aula de orquestração. Para apreciar esta música, o espectador tem que observar outros elementos da música que são magistralmente trabalhados pelo compositor.
